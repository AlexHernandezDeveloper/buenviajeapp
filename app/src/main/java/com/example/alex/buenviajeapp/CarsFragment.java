package com.example.alex.buenviajeapp;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CarsFragment extends Fragment {

    ImageView backgroundWeather, imageWeather, imageWeather2;
    TextView weatherName,grades,uvQuality, airQuality;

    public CarsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_cars, container, false);

        backgroundWeather = (ImageView) view.findViewById(R.id.imgBackground);
        imageWeather = (ImageView) view.findViewById(R.id.imgWeatherTop);
        imageWeather2 = (ImageView) view.findViewById(R.id.imgWeatherBot);
        weatherName = (TextView) view.findViewById(R.id.txtWeather);
        grades = (TextView) view.findViewById(R.id.txtGrades);
        airQuality = (TextView) view.findViewById(R.id.txtAirCondition);
        uvQuality = (TextView) view.findViewById(R.id.txtUV);

        requestWeather(view);

        return view;
    }

    public void requestWeather(final View view){

        final ProgressDialog pDialog = new ProgressDialog(view.getContext());
        String tag_json_obj = "json_obj_req";
        String url = "http://api.labcd.mx/v1/aire/calidad-actual";
        pDialog.setMessage("Loading...");
        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.hide();
                        setInfo(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("--->>>", "Error: " + error.getMessage());
                pDialog.hide();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

        public void setInfo(JSONObject jsonInfo){
    //HELADO <= 0     0 < MUYY FRIO <= 10       10 < FRIO <= 18       18 < TEMPLADO <= 24     24 < CALIDO <= 29 29 < MUY CALIDO
            String []weatherCondition = {"HELADO","MUY FRIO","FRIO","TEMPLADO","CALIDO","MUY CÁLIDO"};
            try {

                if (Integer.parseInt(jsonInfo.getString("gradosclima")) <= 0){
                    //backgroundWeather.setBackgroundColor(Color.parseColor("#1976D2"));
                    imageWeather.setImageResource(R.drawable.cloud_icon);
                    imageWeather2.setImageResource(R.drawable.ice_icon);
                    weatherName.setText(weatherCondition[0]);
                    uvQuality.setText(jsonInfo.getString("caliuvuser"));
                    uvQuality.setBackgroundColor(Color.parseColor(jsonInfo.getString("coliuvuser")));
                    airQuality.setText(jsonInfo.getString("calairno"));
                    airQuality.setBackgroundColor(Color.parseColor(jsonInfo.getString("colairno")));
                    grades.setText(jsonInfo.getString("gradosclima")+"°C");

                }else if (Integer.parseInt(jsonInfo.getString("gradosclima")) <= 10){
                    //backgroundWeather.setBackgroundColor(Color.parseColor("#2196F3"));
                    imageWeather.setImageResource(R.drawable.cloud_icon);
                    imageWeather2.setImageResource(R.drawable.rain_icon);
                    weatherName.setText(weatherCondition[1]);
                    uvQuality.setText(jsonInfo.getString("caliuvuser"));
                    uvQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("coliuvuser")));
                    airQuality.setText(jsonInfo.getString("calairno"));
                    airQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("colairno")));
                    grades.setText(jsonInfo.getString("gradosclima")+"°C");

                }else if (Integer.parseInt(jsonInfo.getString("gradosclima")) <= 18){
                    //backgroundWeather.setBackgroundColor(Color.parseColor("#64B5F6"));
                    imageWeather.setImageResource(R.drawable.cloud_icon);
                    imageWeather2.setImageResource(R.drawable.cloud2_icon);
                    weatherName.setText(weatherCondition[2]);
                    uvQuality.setText(jsonInfo.getString("caliuvuser"));
                    uvQuality.setBackgroundColor(Color.parseColor("#" + jsonInfo.getString("coliuvuser")));
                    airQuality.setText(jsonInfo.getString("calairno"));
                    airQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("colairno")));
                    grades.setText(jsonInfo.getString("gradosclima")+"°C");

                }else if (Integer.parseInt(jsonInfo.getString("gradosclima")) <= 24){
                    //backgroundWeather.setBackgroundColor(Color.parseColor("#FFF176"));
                    imageWeather.setImageResource(R.drawable.cloud_icon);
                    imageWeather2.setImageResource(R.drawable.sun_icon);
                    weatherName.setText(weatherCondition[3]);
                    uvQuality.setText(jsonInfo.getString("caliuvuser"));
                    uvQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("coliuvuser")));
                    airQuality.setText(jsonInfo.getString("calairno"));
                    airQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("colairno")));
                    grades.setText(jsonInfo.getString("gradosclima")+"°C");

                }else if (Integer.parseInt(jsonInfo.getString("gradosclima")) <= 29){
                    //backgroundWeather.setBackgroundColor(Color.parseColor("#FFC107"));
                    imageWeather.setImageResource(R.drawable.cloud2_icon);
                    imageWeather2.setImageResource(R.drawable.sun_icon);
                    weatherName.setText(weatherCondition[4]);
                    uvQuality.setText(jsonInfo.getString("caliuvuser"));
                    uvQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("coliuvuser")));
                    airQuality.setText(jsonInfo.getString("calairno"));
                    airQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("colairno")));
                    grades.setText(jsonInfo.getString("gradosclima")+"°C");

                }else if(Integer.parseInt(jsonInfo.getString("gradosclima")) > 29){
                    //backgroundWeather.setBackgroundColor(Color.parseColor("#FF8F00"));
                    imageWeather.setImageResource(R.drawable.sun_icon);
                    imageWeather2.setImageResource(R.drawable.too_hot_icon);
                    weatherName.setText(weatherCondition[5]);
                    uvQuality.setText(jsonInfo.getString("caliuvuser"));
                    uvQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("coliuvuser")));
                    airQuality.setText(jsonInfo.getString("calairno"));
                    airQuality.setBackgroundColor(Color.parseColor("#"+jsonInfo.getString("colairno")));
                    grades.setText(jsonInfo.getString("gradosclima")+"°C");

                }
            } catch (JSONException e) {e.printStackTrace();}
    }
}
