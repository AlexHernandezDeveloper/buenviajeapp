package com.example.alex.buenviajeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;

public class MainActivity extends AppCompatActivity {

    CallbackManager callbackManager;
    LoginButton loginButton;
    String nameUser, urlUser, idUser, genderUser, emailUser, birthdayUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        loginFacebook();
    }

    public void loginFacebook(){
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("FACEBOOKLOGIIN--->>", "Listo");
                gettingFacebookData(loginResult);
            }
            @Override
            public void onCancel() {
                Log.e("FACEBOOKLOGIIN--->>", "Cancelado");
            }
            @Override
            public void onError(FacebookException exception) {Log.e("FACEBOOKLOGIIN--->>", "Error");}
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void gettingFacebookData(LoginResult loginResult){

        final Bundle arguments = new Bundle();

        arguments.putString("redirect","false");arguments.putString("size","large");
        arguments.putString("height","400");arguments.putString("width", "400");

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            final String email = object.getString("email");
                            final String birthday = object.getString("birthday");
                            final String id = object.getString("id");
                            final String name = object.getString("name");
                            final String gender = object.getString("gender");
                            new GraphRequest(
                                    AccessToken.getCurrentAccessToken(),
                                    "/"+id+"/picture",
                                    arguments,
                                    HttpMethod.GET,
                                    new GraphRequest.Callback() {
                                        public void onCompleted(GraphResponse response) {
                                            try {
                                                JSONObject jsonObject = response.getJSONObject();
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                                urlUser = jsonObject1.getString("url");
                                                emailUser = email;
                                                birthdayUser = birthday;
                                                nameUser = name;
                                                genderUser = gender;
                                                /*Log.e("----->>", urlUser);
                                                Log.e("----->>", emailUser);
                                                Log.e("----->>", birthdayUser);
                                                Log.e("----->>", nameUser);
                                                Log.e("----->>", genderUser);*/
                                                Intent intent = new Intent(MainActivity.this,NavigationDrawerActivity.class);
                                                intent.putExtra("urlUser",urlUser);
                                                intent.putExtra("name",nameUser);
                                                intent.putExtra("email", emailUser);
                                                intent.putExtra("gender", genderUser);
                                                intent.putExtra("birthday", birthdayUser);
                                                startActivity(intent);
                                            } catch (JSONException e) {e.printStackTrace();}
                                        }
                                    }
                            ).executeAsync();
                        } catch (JSONException e) {e.printStackTrace();}
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
